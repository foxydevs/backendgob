<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('accesos', 'AccesosController');
Route::resource('alertas', 'AlertasController');
Route::resource('eventofotos', 'EventoFotosController');
Route::resource('eventos', 'EventosController');
Route::resource('eventousuarios', 'EventoUsuariosController');
Route::resource('mensajes', 'MensajesController');
Route::resource('modulos', 'ModulosController');
Route::resource('posiciones', 'PosicionesController');
Route::resource('puestos', 'PuestosController');
Route::resource('roles', 'RolesController');
Route::resource('tipoeventos', 'TipoEventosController');
Route::resource('usuarios', 'UsuariosController');

Route::post('usuarios/{id}/upload/avatar', 'UsuariosController@uploadAvatar');
Route::post('eventos/{id}/upload/avatar', 'EventosController@uploadAvatar');
Route::post('eventos/{id}/upload/picture/{id2}', 'EventoFotosController@uploadAvatar');
Route::post('usuarios/{id}/changepassword', 'UsuariosController@changePassword');
Route::post('usuarios/password/reset', 'UsuariosController@recoveryPassword');

Route::get('atendidas/alertas', 'AlertasController@getAtendidas');
Route::get('atendidas/{id}/alertas', 'AlertasController@getAtendidasByUser');
Route::get('eventos/{id}/participantes', 'EventosController@getUsuariosEvento');
Route::get('poratender/alertas', 'AlertasController@getPorAtender');
Route::get('poratender/{id}/alertas', 'AlertasController@getPorAtenderByUser');
Route::get('usuarios/{id}/modulos', 'AccesosController@getAccesos');
Route::get('usuarios/{id}/modulos/{id2}', 'AccesosController@getAcceso');

Route::post('login', 'AuthenticateController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
