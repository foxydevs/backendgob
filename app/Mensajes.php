<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajes extends Model
{
    protected $table = 'mensajes';

    public function recibe(){
        return $this->hasOne('App\Usuarios', 'id', 'receptor');
    }

    public function envia(){
        return $this->hasOne('App\Usuarios', 'id', 'emisor');
    }
}
