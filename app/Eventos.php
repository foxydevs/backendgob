<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Eventos extends Model
{
    use SoftDeletes;
    protected $table = 'eventos';

    public function usuarios() {
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }

    public function asistentes() {
        return $this->hasMany('App\EventoUsuarios', 'evento')->where('estado','1')->with('usuarios');
    }

    public function interesados() {
        return $this->hasMany('App\EventoUsuarios', 'evento')->where('estado','0')->with('usuarios');
    }

    public function fotos() {
        return $this->hasMany('App\EventoFotos', 'evento');
    }
}
