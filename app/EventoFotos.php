<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoFotos extends Model
{
    use SoftDeletes;
    protected $table = 'evento_foto';
}
