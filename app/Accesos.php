<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accesos extends Model
{
    protected $table = 'accesos';

    public function modulos(){
        return $this->hasOne('App\Modulos','id','modulo');
    }
}
