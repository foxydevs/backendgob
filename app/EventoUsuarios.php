<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoUsuarios extends Model
{
    use SoftDeletes;
    protected $table = 'evento_usuario';

    public function usuarios(){
        return $this->hasOne('App\Usuarios', 'id', 'usuario');
    }

    public function eventos(){
        return $this->hasOne('App\Eventos', 'id', 'evento');
    }
}
