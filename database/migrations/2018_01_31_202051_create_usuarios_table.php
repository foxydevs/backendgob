<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->nullable()->default(null);
            $table->string('password');
            $table->string('email')->nullable()->default(null);
            $table->string('nombre')->nullable()->default(null);
            $table->string('apellido')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->string('edad')->nullable()->default(null);
            $table->date('nacimiento')->nullable()->default(null);
            $table->string('telefono')->nullable()->default(null);
            $table->string('picture')->default('http://static1.squarespace.com/static/56460adde4b037db841d48d2/56f05542d51cd4973a4c737c/56f2fbef62cd94748f483395/1460425942911/no_avatar_man.gif?format=1000w');
            $table->double('ultima_latitud',15,8)->nullable()->default(null);
            $table->double('ultima_longitud',15,8)->nullable()->default(null);
            $table->integer('privileges')->default(1);
            $table->integer('estado')->default(1);

            $table->integer('rol')->unsigned()->nullable()->default(null);
            $table->foreign('rol')->references('id')->on('roles')->onDelete('cascade');

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
