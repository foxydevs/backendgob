<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mensaje')->nullable()->default(null);
            $table->string('asunto')->nullable()->default(null);
            $table->timestamp('fecha')->useCurrent();
            $table->timestamp('leido')->useCurrent();
            $table->integer('estado')->default(1);
            
            $table->integer('emisor')->unsigned()->nullable()->default(null);
            $table->foreign('emisor')->references('id')->on('usuarios');
            $table->integer('receptor')->unsigned()->nullable()->default(null);
            $table->foreign('receptor')->references('id')->on('usuarios');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes');
    }
}
