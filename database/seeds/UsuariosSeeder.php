<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'id'               => 1,
            'username'         => "admin",
            'password'         => bcrypt('foxylabs'),
            'email'            => "admin@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 2,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'id'               => 2,
            'username'         => "daniel",
            'password'         => bcrypt('foxylabs'),
            'email'            => "daniel@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 1,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'id'               => 3,
            'username'         => "alejandro",
            'password'         => bcrypt('foxylabs'),
            'email'            => "alejandro@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 3,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('eventos')->insert([
            "fecha"            => "2018-02-02",
            "hora"             => "15:31:03",
            "lugar_id"         => null,
            "lugar"            => "foxyLabs",
            "direccion"        => "3ra Avenida 20-06 zona 2",
            "picture"          => "https://berp.s3.us-west-2.amazonaws.com/eventos/PgF5AxVQSB04YN46EHSA3aW4nnnxJ6N3yHOSpAct.jpeg",
            "descripcion"      => "Evento prioritario",
            "latitud"          => 14.65177763,
            "longitud"         => -90.5139783,
            "estado"           => 1,
            "tipo"             => null,
            "usuario"          => null,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('evento_usuario')->insert([
            "estado"           => 1,
            "evento"           => 1,
            "usuario"          => 2,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);
        
        DB::table('evento_usuario')->insert([
            "estado"           => 1,
            "evento"           => 1,
            "usuario"          => 1,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);
        
        DB::table('evento_usuario')->insert([
            "estado"           => 0,
            "evento"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('alertas')->insert([
            "descripcion"      => "Alerta de Peligro",
            "direccion"        => null,
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.66430814,
            "longitud"         => -90.51446915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 1,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('alertas')->insert([
            "descripcion"      => "Alerta de Peligro",
            "direccion"        => null,
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.61139814,
            "longitud"         => -90.51446915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 2,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('alertas')->insert([
            "descripcion"      => "Alerta de Peligro",
            "direccion"        => null,
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.61139814,
            "longitud"         => -90.50446959,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion de Admin",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.66439814,
            "longitud"         => -90.51446915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 2,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion de Admin",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.66359814,
            "longitud"         => -90.61646915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 2,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion Enviada",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.64369814,
            "longitud"         => -90.61646915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion Enviada",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.64379814,
            "longitud"         => -90.61746915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion Enviada",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.65359814,
            "longitud"         => -90.62546915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion Enviada",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.68359814,
            "longitud"         => -90.65546915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion Enviada",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.69359814,
            "longitud"         => -90.60546915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);

        DB::table('posiciones')->insert([
            "descripcion"      => "Ubicacion Enviada",
            "fecha"            => date('Y-m-d H:m:s'),
            "latitud"          => 14.65359814,
            "longitud"         => -90.69546915,
            "imei"             => null,
            "estado"           => 1,
            "usuario"          => 3,
            "deleted_at"       => null,
            "created_at"       => date('Y-m-d H:m:s'),
            "updated_at"       => date('Y-m-d H:m:s')
        ]);
    }
}
