<?php

use Illuminate\Database\Seeder;

class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modulos')->insert([
            "nombre"=>"Dashboard",
            "icono"=>"tachometer",
            "link"=>"dashboard",
            "dir"=>"../app/img/inicio.png",
            "refId"=>"inicio",
            "orden"=>1,
            "tipo"=>0,
            "estado"=>1,
            "created_at"=>date('Y-m-d H:m:s'),
            "updated_at"=>date('Y-m-d H:m:s')
        ]);
        DB::table('modulos')->insert([
            "nombre"=>"Usuarios",
            "icono"=>"users",
            "link"=>"usuarios",
            "dir"=>"../app/img/usuariotab.png",
            "refId"=>"usuario",
            "orden"=>3,
            "tipo"=>0,
            "estado"=>1,
            "created_at"=>date('Y-m-d H:m:s'),
            "updated_at"=>date('Y-m-d H:m:s')
        ]);
        DB::table('modulos')->insert([
            "nombre"=>"Alertas",
            "icono"=>"exclamation-triangle",
            "link"=>"alertas",
            "dir"=>null,
            "refId"=>null,
            "orden"=>4,
            "tipo"=>0,
            "estado"=>1,
            "created_at"=>date('Y-m-d H:m:s'),
            "updated_at"=>date('Y-m-d H:m:s')
        ]);
        DB::table('modulos')->insert([
            "nombre"=>"Ubicaciones",
            "icono"=>"map-marker",
            "link"=>"ubicaciones",
            "dir"=>null,
            "refId"=>null,
            "orden"=>5,
            "tipo"=>0,
            "estado"=>1,
            "created_at"=>date('Y-m-d H:m:s'),
            "updated_at"=>date('Y-m-d H:m:s')
        ]);
        DB::table('modulos')->insert([
            "nombre"=>"Eventos",
            "icono"=>"calendar",
            "link"=>"eventos",
            "dir"=>null,
            "refId"=>null,
            "orden"=>3,
            "tipo"=>0,
            "estado"=>1,
            "created_at"=>date('Y-m-d H:m:s'),
            "updated_at"=>date('Y-m-d H:m:s')
        ]);
        DB::table('modulos')->insert([
            "nombre"=>"Participantes",
            "icono"=>"group",
            "link"=>"eventos-usuarios",
            "dir"=>null,
            "refId"=>null,
            "orden"=>3,
            "tipo"=>0,
            "estado"=>1,
            "created_at"=>date('Y-m-d H:m:s'),
            "updated_at"=>date('Y-m-d H:m:s')
        ]);
    }
}
